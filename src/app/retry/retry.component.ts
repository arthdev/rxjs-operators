import { Component, OnInit } from '@angular/core';
import { catchError, interval, map, mergeMap, of, retry, take, throwError } from 'rxjs';

@Component({
  selector: 'app-retry-component',
  templateUrl: './retry.component.html',
  styleUrls: []
})
export class RetryComponent implements OnInit {
    ngOnInit(): void {
        const source = interval(1000);
        const result = source.pipe(
            mergeMap(val => val > 2 ? throwError(() => 'Error!') : of(val)),
            // retry 2 times on error
            retry(2),
        );
        
        result.subscribe({
            next: value => console.log(value),
            error: err => console.log(`${ err }: Retried 2 times then quit!`)
        });
 
// Output:
// 0..1..2..
// 0..1..2..
// 0..1..2..
// 'Error!: Retried 2 times then quit!'
    }
}