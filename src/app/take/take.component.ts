import { Component, OnInit } from '@angular/core';
import { of, take } from 'rxjs';

@Component({
  selector: 'app-take-component',
  templateUrl: './take.component.html',
  styleUrls: []
})
export class TakeComponent implements OnInit {
    ngOnInit(): void {
        const obs = of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        obs.pipe(
            take(5),
        )
        .subscribe(v => console.log(v));
    }
}
