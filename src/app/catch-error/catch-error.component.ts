import { Component, OnInit } from '@angular/core';
import { catchError, map, of, take } from 'rxjs';

@Component({
  selector: 'app-catch-error-component',
  templateUrl: './catch-error.component.html',
  styleUrls: []
})
export class CatchErrorComponent implements OnInit {
    ngOnInit(): void {
        const obs = of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        obs.pipe(
            map(n => {
              if (n === 4) {
                throw 'four!';
              }
              return n;
            }),
            catchError((err, caught) => {
                console.error(err);
                return caught
            }),
            take(8)
          )
          .subscribe(x => console.log(x));
    }
}
