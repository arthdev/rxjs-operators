import { Component, OnInit } from '@angular/core';
import { debounceTime, fromEvent, of, take } from 'rxjs';

@Component({
  selector: 'app-debouce-time-component',
  templateUrl: './debouce-time.component.html',
  styleUrls: []
})
export class DebouceTimeComponent implements OnInit {
    ngOnInit(): void {
        const clicks = fromEvent(document, 'click');
        const result = clicks.pipe(
            debounceTime(1000)
        );
        result.subscribe(x => console.log(x))
    }
}
