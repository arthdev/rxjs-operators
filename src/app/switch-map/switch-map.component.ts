import { Component, OnInit } from '@angular/core';
import { debounceTime, fromEvent, interval, of, switchMap, take } from 'rxjs';

@Component({
  selector: 'app-switch-map-component',
  templateUrl: './switch-map.component.html',
  styleUrls: []
})
export class SwitchMapComponent implements OnInit {
    ngOnInit(): void {
        const clicks = fromEvent(document, 'click');
        const result = clicks.pipe(
            switchMap(() => interval(500))
        );
        result.subscribe(x => console.log(x));
    }
}
