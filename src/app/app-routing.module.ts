import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { take } from 'rxjs';
import { CatchErrorComponent } from './catch-error/catch-error.component';
import { DebouceTimeComponent } from './debouce-time/debouce-time.component';
import { MapsComponent } from './maps/maps.component';
import { RetryComponent } from './retry/retry.component';
import { SwitchMapComponent } from './switch-map/switch-map.component';
import { TakeComponent } from './take/take.component';

const routes: Routes = [
  {
    path: '',
    component: TakeComponent,
  },
  {
    path: 'take',
    component: TakeComponent,
  },
  {
    path: 'catch-error',
    component: CatchErrorComponent,
  },
  {
    path: 'retry',
    component: RetryComponent,
  },
  {
    path: 'debounce-time',
    component: DebouceTimeComponent,
  },
  {
    path: 'switch-map',
    component: SwitchMapComponent,
  },
  {
    path: 'maps',
    component: MapsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
