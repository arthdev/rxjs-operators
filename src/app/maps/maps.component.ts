import { Component, OnInit } from '@angular/core';
import { concatMap, debounceTime, delay, fromEvent, interval, mergeMap, of, OperatorFunction, switchMap, take } from 'rxjs';

@Component({
  selector: 'app-maps-component',
  templateUrl: './maps.component.html',
  styleUrls: []
})
export class MapsComponent{
    source()  {
        return of(1,2,3,4,5);
    }

    mergeMap() {
        this.source()
            .pipe(
                mergeMap(v => {
                    console.log('creating new observable');
                    return of(v).pipe(delay(1000))
                })
            )
            .subscribe(v => console.log(v));
    }

    switchMap() {
        this.source()
            .pipe(
                switchMap(v => {
                    console.log('creating new observable');
                    return of(v).pipe(delay(1000))
                })
            )
            .subscribe(v => console.log(v));
    }

    concatMap() {
        this.source()
        .pipe(
            concatMap(v => {
                    console.log('creating new observable');
                    return of(v).pipe(delay(1000))
                })
        )
        .subscribe(v => console.log(v));
    }
}
